class CustomComponent extends HTMLElement {
  _style = document.createElement('style');
  _class = document.createElement('class');
  _link = document.createElement('link');
  _div = document.createElement('div');

  constructor() {
    super();

    // this._defineStyle();
    // this._defineClass();
    this._defineLink();
    // this._defineDiv();
    this._attachToShadowDOM();
    // this.data = {};
    // this._pdate();
  }
  _defineLink() {
    this._link.setAttribute('rel', 'stylesheet');
    this._link.setAttribute('href', 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css');
  }

  // _defineStyle() {
  //   this._style.innerHTML = `
  //   h1 {
  //     font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
  //       Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
  //       'Segoe UI Symbol';
  //   }
  //   `;
  // }

  // _defineClass() {
  //   this._class.setAttribute('class',"text-green-500 text-2xl md:text-3xl lg:text-4xl font-bold p-4");
  // }


  _defineDiv() {

    this._div.textContent = this.getAttribute('data-text');
    this._div.id = '1'
    // this._div.textContent='heklo'

    //     this._div.innerHTML = `

    //     Custom Web Component

    // `;
    this._div.setAttribute('class', 'text-green-500 text-2xl md:text-3xl lg:text-4xl font-bold p-4')

  }

  _attachToShadowDOM() {
    this.attachShadow({
      mode: 'open'
    });
    // this.shadowRoot.appendChild(this._style);
    this.shadowRoot.appendChild(this._link)
    // this.shadowRoot.appendChild(this._template.content.cloneNode(true));
    // this.shadowRoot.appendChild(this._div);
  }
  connectedCallback() {
    // 

    var mthis = this;


    function KnowledgeModel() {
      //Model API
      // let dt = this
      this.load = async id => {
        let res = await fetch(`https://cvvuewebgo.vercel.app/api/post`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'To-Do': 'knowlege'
          }
        })
        // let res = await fetch( `https://reqres.in/api/users/8` )

        let json = await res.json()
        let knows = Array.from(json)

        Object.assign(this, knows)
        pdate(mthis);
        // controller.updateView()
      }
    }

    let model = new KnowledgeModel
    model.load()

    function pdate(mthis) {


      let objlength = Object.keys(model).length

      let contdiv = document.createElement('div');
      contdiv.className = 'container grid grid-cols-3 gap-5 mx-auto'

      for (let i = 0; i < objlength - 1; i++) {

        
        let divimg = document.createElement('div');
        divimg.className = 'w-full rounded'

        let img = document.createElement('img');
        let imgsrc = `https://www.remotejob.eu/images/${model[i].img}`
        img.src = imgsrc
        
        divimg.appendChild(img);
        contdiv.appendChild(divimg)

      }
  

      mthis.shadowRoot.appendChild(contdiv)


    }


  }


}


customElements.define('custom-component', CustomComponent);
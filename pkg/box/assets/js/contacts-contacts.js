class ContactsElement extends HTMLElement {

    // _link = document.createElement('link');

    constructor() {
        super();

        this.locale = document.documentElement.getAttribute('lang') || 'fi-FI';

        // this._defineLink();
        // this._defineDiv();
        // this._attachToShadowDOM();
        // this.data={};
    }

    // _defineLink() {
    //     this._link.setAttribute('rel', 'stylesheet');
    //     this._link.setAttribute('href', 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css');
    // }

    //   _defineDiv() {

    //     this._div.textContent=this.getAttribute('data-text');
    //     // this._div.id='card'
    //     // this._div.textContent='heklo'

    // //     this._div.innerHTML = `

    // //     Custom Web Component

    // // `;
    //     this._div.setAttribute('class', 'text-green-500 text-2xl md:text-3xl lg:text-4xl font-bold p-4')

    //   }

    // _attachToShadowDOM() {
    //     this.attachShadow({
    //         mode: 'open'
    //     });
    //     // this.shadowRoot.appendChild(this._style);
    //     // this.shadowRoot.appendChild(this._link)
    //     // // this.shadowRoot.appendChild(this._template.content.cloneNode(true));
    //     // this.shadowRoot.appendChild(this._div);
    //     this.shadowRoot.append(this._link)
    // }

    connectedCallback() {
        let card = this
        // let head = this
        // let elem = this


        function ContactsView() {

            let elem = document.createElement('div');

            elem.innerHTML = `
 



  <div class="container mx-auto">
 

<div class="flex flex-wrap text-sm">
  <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 mb-4 flex justify-center border-2 border-gray-300 rounded-xl p-6 bg-gray-50">
  
  
  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
</svg>

            +358451202810
  
  </div>
  <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 mb-4 flex justify-center border-2 border-gray-300 rounded-xl p-6 bg-gray-50">
  
  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 19v-8.93a2 2 0 01.89-1.664l7-4.666a2 2 0 012.22 0l7 4.666A2 2 0 0121 10.07V19M3 19a2 2 0 002 2h14a2 2 0 002-2M3 19l6.75-4.5M21 19l-6.75-4.5M3 10l6.75 4.5M21 10l-6.75 4.5m0 0l-1.14.76a2 2 0 01-2.22 0l-1.14-.76" />
</svg>

<a class="no-underline hover:underline" href="mailto:aleksander.mazurov@gmail.com"> aleksander.mazurov@gmail.com</pre></a>   
  </div>
  <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 mb-4 flex justify-center border-2 border-gray-300 rounded-xl p-6 bg-gray-50">


  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
</svg>
<a href="https://gitlab.com/remotejob">https://gitlab.com/remotejob</a>
  
  </div>
  <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 mb-4 flex justify-center border-2 border-gray-300 rounded-xl p-6 bg-gray-50 ">

  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
</svg>

<a href="https://github.com/remotejob">https://github.com/remotejob</a>   
  
  </div>
 
</div>



       
            `

            this.update = () => {

                

                // const btn = document.querySelector("button.mobile-menu-button");
                // const menu = document.querySelector(".mobile-menu");

                // // Add Event Listeners
                // btn.addEventListener("click", () => {
                //     menu.classList.toggle("hidden");
                // });

            }


            // card.shadowRoot.appendChild(elem)
            card.appendChild(elem)

        }

        let view = new ContactsView

        this.init = () => view.update()

        this.init()


    }
}

customElements.define('contacts-contacts', ContactsElement)
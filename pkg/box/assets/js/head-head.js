class HeadElement extends HTMLElement {

    // _link = document.createElement('link');

    constructor() {
        super();

        this.locale = document.documentElement.getAttribute('lang') || 'fi-FI';

        // this._defineLink();
        // this._attachToShadowDOM();
        // this.data={};
    }

    // _defineLink() {
    //     this._link.setAttribute('rel', 'stylesheet');
    //     this._link.setAttribute('href', 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css');
    // }

    // _attachToShadowDOM() {
    //     this.attachShadow({
    //         mode: 'open'
    //     });
    //     // this.shadowRoot.append(this._link)
    // }


    connectedCallback() {

        // let head = this
        let elem = this

        function HeadView() {

            elem.innerHTML = `
            <nav class="bg-white shadow-lg">
            <div class="max-w-6xl mx-auto px-4">
                <div class="flex justify-between">
                    <div class="flex space-x-7">
                        <div>
                            <!-- Website Logo -->
                            <a href="#" class="flex items-center py-4 px-2">
                                <img src="assets/imgs/mazurovopt.jpg" alt="Logo" class="h-10 mr-2">
                                <span class="font-semibold text-gray-500 text-lg">Remotejob</span>
                            </a>
                        </div>
                        <!-- Primary Navbar items -->
                        <div class="hidden md:flex items-center space-x-1">
                            <a  id ='mainlink' class="py-4 px-2 text-gray-500 border-green-500 font-semibold hover:text-green-500 transition duration-300">Home</a>
                            <a  id ='knowledgelink'
                                class="py-4 px-2 text-gray-500 font-semibold border-green-500 hover:text-green-500 transition duration-300">Knowledge</a>
                            <a id ='contactslink'
                                class="py-4 px-2 text-gray-500 font-semibold border-green-500 hover:text-green-500 transition duration-300">Contacts
                                </a>
                        </div>
                    </div>
                    <!-- Secondary Navbar items -->
                    <!-- <div class="hidden md:flex items-center space-x-3 ">
                        <a href=""
                            class="py-2 px-2 font-medium text-gray-500 rounded hover:bg-green-500 hover:text-white transition duration-300">Log
                            In</a>
                        <a href=""
                            class="py-2 px-2 font-medium text-white bg-green-500 rounded hover:bg-green-400 transition duration-300">Sign
                            Up</a>
                    </div>-->
                    <!-- Mobile menu button -->
                    <div class="md:hidden flex items-center">
                        <button class="outline-none mobile-menu-button">
                            <svg class=" w-6 h-6 text-gray-500 hover:text-green-500 " x-show="!showMenu" fill="none"
                                stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path d="M4 6h16M4 12h16M4 18h16"></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <!-- mobile menu -->
            <div class="hidden mobile-menu">
                <ul class="">
                    <li class="active"><a id ='mainlinkmob'
                            class="block text-sm px-2 py-4 text-white bg-green-500 font-semibold">Home</a></li>
                    <li><a  id ='knowledgelinkmob'
                            class="block text-sm px-2 py-4 hover:bg-green-500 transition duration-300">Knowledge</a></li>
                    <li><a id ='contactslinkmob'
                            class="block text-sm px-2 py-4 hover:bg-green-500 transition duration-300">Contacts</a></li>
                </ul>
            </div>
    
        </nav>

            `

            this.update = () => {

                const btn = document.querySelector("button.mobile-menu-button");
                const menu = document.querySelector(".mobile-menu");

                const main = document.getElementById('main');
                const mainlinkmob = document.getElementById('mainlinkmob');
                const mainlink = document.getElementById('mainlink');


                const knowledge = document.getElementById('knowledge');
                const knowledgelinkmob = document.getElementById('knowledgelinkmob');
                const knowledgelink = document.getElementById('knowledgelink');


                const contacts = document.getElementById('contacts');
                const contactslinkmob = document.getElementById('contactslinkmob');
                const contactslink = document.getElementById('contactslink');


                mainlink.classList.toggle('border-b-4')
                knowledge.classList.add("hidden")
                contacts.classList.add("hidden")


                btn.addEventListener("click", () => {
                    menu.classList.toggle("hidden");

                    if (!main.classList.contains("hidden")) {

                        main.classList.add("hidden")

                    }

                    if (!knowledge.classList.contains("hidden")) {

                        knowledge.classList.add("hidden")

                    }


                });

                mainlink.addEventListener("click", () => {


                    if (main.classList.contains("hidden")) {

                        main.classList.remove("hidden")
                        mainlink.classList.toggle('border-b-4')


                    }

                    if (!knowledge.classList.contains("hidden")) {

                        
                        knowledge.classList.add("hidden")
                        knowledgelink.classList.toggle('border-b-4')

                    }


                    if (!contacts.classList.contains("hidden")) {

                        
                        contacts.classList.add("hidden")
                        contactslink.classList.toggle('border-b-4')

                    }

                });

                knowledgelink.addEventListener("click", () => {

                    if (knowledge.classList.contains("hidden")) {

                        knowledge.classList.remove("hidden")
                        knowledgelink.classList.toggle('border-b-4')

                    }


                    if (!main.classList.contains("hidden")) {

                        
                        main.classList.add("hidden")
                        mainlink.classList.toggle('border-b-4')

                    }


                    if (!contacts.classList.contains("hidden")) {

                        contacts.classList.add("hidden")
                        contactslink.classList.toggle('border-b-4')

                    }

                });


                contactslink.addEventListener("click", () => {


                    if (contacts.classList.contains("hidden")) {

                     
                        contacts.classList.remove("hidden")
                        contactslink.classList.toggle('border-b-4')

                    }

                    if (!main.classList.contains("hidden")) {

                       
                        main.classList.add("hidden")
                        mainlink.classList.toggle('border-b-4')


                    }

                    if (!knowledge.classList.contains("hidden")) {

                        knowledge.classList.add("hidden")
                        knowledgelink.classList.toggle('border-b-4')

                    }


                });

                mainlinkmob.addEventListener("click", () => {
   
                    menu.classList.toggle("hidden");

                    if (main.classList.contains("hidden")) {

                        main.classList.remove("hidden")
                    }

                    if (!knowledge.classList.contains("hidden")) {

                        knowledge.classList.add("hidden")

                    }

                    if (!contacts.classList.contains("hidden")) {

    
                        contacts.classList.add("hidden")

                    }


                });

                knowledgelinkmob.addEventListener("click", () => {


                    menu.classList.toggle("hidden");


                    if (knowledge.classList.contains("hidden")) {


                        knowledge.classList.remove("hidden")

                    }


                    if (!main.classList.contains("hidden")) {


                        main.classList.add("hidden")

                    }


                    if (!contacts.classList.contains("hidden")) {

                        contacts.classList.add("hidden")

                    }

                });

                contactslinkmob.addEventListener("click", () => {


                    menu.classList.toggle("hidden");

                    if (contacts.classList.contains("hidden")) {

                        contacts.classList.remove("hidden")

                    }

                    if (!main.classList.contains("hidden")) {

                        main.classList.add("hidden")

                    }

                    if (!knowledge.classList.contains("hidden")) {

                        knowledge.classList.add("hidden")

                    }

                });

            }

        }

        let view = new HeadView
        this.init = () => view.update()

        this.init()

    }

}

customElements.define('head-head', HeadElement)
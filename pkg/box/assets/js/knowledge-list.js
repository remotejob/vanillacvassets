//https://stackoverflow.com/questions/60080367/javascript-mvc-with-custom-html-elements-notifying-controller

//CONTROLLER
class KnowledgeController extends HTMLElement {

    // _link = document.createElement('link');

    constructor() {
        super();

        this.locale = document.documentElement.getAttribute('lang') || 'fi-FI';

        // this._defineLink();
        // this._defineDiv();
        // this._attachToShadowDOM();
        // this.data={};
    }

    // _defineLink() {
    //     this._link.setAttribute('rel', 'stylesheet');
    //     this._link.setAttribute('href', 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css');
    // }

    //   _defineDiv() {

    //     this._div.textContent=this.getAttribute('data-text');
    //     // this._div.id='card'
    //     // this._div.textContent='heklo'

    // //     this._div.innerHTML = `

    // //     Custom Web Component

    // // `;
    //     this._div.setAttribute('class', 'text-green-500 text-2xl md:text-3xl lg:text-4xl font-bold p-4')

    //   }

    // _attachToShadowDOM() {
    //     this.attachShadow({
    //         mode: 'open'
    //     });
    //     // this.shadowRoot.appendChild(this._style);
    //     // this.shadowRoot.appendChild(this._link)
    //     // // this.shadowRoot.appendChild(this._template.content.cloneNode(true));
    //     // this.shadowRoot.appendChild(this._div);
    //     this.shadowRoot.append(this._link)
    // }

    connectedCallback() {

        let card = this
        let controller = this

        // let elem = this

        //MODEL
        function KnowledgeModel() {
            //Model API
            this.load = async id => {
                let res = await fetch(`https://cvvuewebgo.vercel.app/api/post`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'To-Do': 'knowlege'
                    }
                })
                // let res = await fetch( `https://reqres.in/api/users/8` )

                let json = await res.json()
                let knows = Array.from(json)

                Object.assign(this, knows)
                controller.updateView()
            }
        }

        let model = new KnowledgeModel

        //VIEW
        function KnowledgeView() {
            // elem.innerHTML = `

            //     <div id=card></div>`

            // let card = elem.querySelector('div#card')
            // let cart = this.shadowRoot
            // let card = document.getElementById('card')
            // let index = elem.querySelector( 'input#index' )

            // index.onchange = () => controller.init( index.value)               

            //View API
            this.update = () => {

                let objlength = Object.keys(model).length

                let contdiv = document.createElement('div');

                
                contdiv.className = 'container grid grid-cols-3 gap-5 mx-auto'

                for (let i = 0; i < objlength - 1; i++) {

                    let divimg = document.createElement('div');
                    divimg.className = 'w-full rounded'

                    let img = document.createElement('img');
                    let imgsrc = `https://www.remotejob.eu/images/${model[i].img}`
                    img.src = imgsrc

                    divimg.appendChild(img);
                    contdiv.appendChild(divimg)

                }
                // card.shadowRoot.appendChild(contdiv)
                card.appendChild(contdiv)

            }
        }

        let view = new KnowledgeView

        //Controller API
        this.init = () => model.load()
        this.updateView = () => view.update()

        //Init with some data 
        this.init()
    }
}

customElements.define('knowledge-list', KnowledgeController)